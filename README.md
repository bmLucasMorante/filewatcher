# fileWatcher
![app_preview.png](/resources/app_preview.png)

## Description
Tool to make sure all the renders are unzipped as they come and update the status in SG.


## Getting started
To start the app us `launch.bat` or the command line to have more control.

`python monitor_rich.py -tui`


## Installation
Clone the repo in the Desktop.

`git clone https://gitlab.com/bmLucasMorante/filewatcher.git`

Run `install.bat` to create a venv and download the dependencies.

### Manual Install
```
cd <repo_path>
python -m venv .venv
.venv/scripts/activate
pip install -r requirements.txt
```

## Usage

```python monitor_rich.py -tui```

-tui: Used to start the app in Text User Interface mode

-p: to provide a path to listen for file events

-threads: to specify the number of threads to use