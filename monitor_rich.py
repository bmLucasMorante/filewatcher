import os
import time
import argparse
import datetime
import keyboard

from rich.console import Console
from rich.table import Table
from rich.live import Live
from rich.layout import Layout
from rich.text import Text

from monitor import Scheduler, BaseJob, MONITOR_LOG_FILE


console = Console()


def normalize_path(path):
    return os.path.normpath(path).replace("\\", "/")


def display_scheduler(scheduler):
    start_time = datetime.datetime.now()
    date_format = '%Y-%m-%d %H:%M:%S'
    MAX_DISPLAY_QUEUE_ROWS = 10

    layout = Layout()
    layout.split(
        Layout(name="header", size=1),
        Layout(name="body"),
        Layout(name="footer", size=6),
    )

    def increase_max_threads():
        if scheduler.MAXIMUM_THREADS < scheduler.MAXIMUM_THREADS + 1:
            scheduler.MAXIMUM_THREADS += 1

    def decrease_max_threads():
        if scheduler.MAXIMUM_THREADS > 0:
            scheduler.MAXIMUM_THREADS -= 1

    def enable_close_thread_after_task():
        scheduler.CLOSE_THREAD_AFTER_TASK = True

    def disable_close_thread_after_task():
        scheduler.CLOSE_THREAD_AFTER_TASK = False

    keyboard.add_hotkey('ctrl+up', increase_max_threads)
    keyboard.add_hotkey('ctrl+down', decrease_max_threads)
    keyboard.add_hotkey('ctrl+right', enable_close_thread_after_task)
    keyboard.add_hotkey('ctrl+left', disable_close_thread_after_task)

    with Live(layout, refresh_per_second=1, screen=True):
        while True:
            current_time = datetime.datetime.now()
            time_running = current_time - start_time
            days = time_running.days
            hours, remainder = divmod(time_running.seconds, 3600)
            minutes, _ = divmod(remainder, 60)
            formatted_difference = f"{days} Days {hours} Hours {minutes} Minutes"

            header_text = Text("Event Processor Activity Monitor", justify="left")
            header_text.stylize(style="reverse white")
            layout["header"].update(header_text)

            body_table = Table(expand=True, show_edge=True)
            body_table.add_column("Info", justify="left")

            body_table.add_row(f"Running since: {start_time.strftime(date_format)}  ({formatted_difference})")
            body_table.add_row(f"TOTAL_JOBS_COMPLETED: {len(scheduler._total_jobs_completed)}")
            body_table.add_row(f"FAILED_JOBS: {len(scheduler._failed_jobs)}")
            body_table.add_row("")
            body_table.add_row(f"MAXIMUM_THREADS: {scheduler.MAXIMUM_THREADS}")
            body_table.add_row(f"CLOSE_THREAD_AFTER_TASK: {scheduler.CLOSE_THREAD_AFTER_TASK}")
            body_table.add_row(f"ROOT_PATH: {normalize_path(scheduler.ROOT_PATH)}")
            body_table.add_row(f"LOG_PATH: {normalize_path(MONITOR_LOG_FILE)}")
            body_table.add_row("")

            if scheduler.leftovers_thread.is_alive():
                body_table.add_row(f"[{scheduler.THREAD_NAME_LEFTOVERS}] Looking for unprocessed files...")
                body_table.add_row("")
            if scheduler.found_leftovers:
                body_table.add_row(f"[{scheduler.THREAD_NAME_LEFTOVERS}] Found items: {len(scheduler.found_leftovers)}")
                body_table.add_row("")

            if scheduler.observer.is_alive():
                body_table.add_row(f"[{scheduler.OBSERVER_NAME_LISTENER}] Listening for new files...")
                body_table.add_row("")

            msg = f"{len(scheduler.jobs.queue)} Items." if scheduler.jobs.queue else "Empty"
            body_table.add_row(f"[Queue] - {msg}")
            if not scheduler.jobs.empty():
                for job in sorted(scheduler.jobs.queue, key=lambda x: x.name)[:MAX_DISPLAY_QUEUE_ROWS]:
                    body_table.add_row(Text(f"{job.data['file'].as_posix()}", style="cyan"))
            body_table.add_row("")

            if scheduler.distributor_thread.is_alive():
                msg = f"Workers: {len(scheduler.threads[:])}"
                body_table.add_row(f"[{scheduler.THREAD_NAME_DISTRIBUTOR}] {msg}")

            for thread_name, job in scheduler.thread_jobs.items():
                job = scheduler.thread_jobs.get(thread_name, None)
                if job and isinstance(job, BaseJob):
                    body_table.add_row(Text(f"[{thread_name}] - {job.data['file'].as_posix()}", style="green"))
                    report_msg = job.reports[-1] if job.reports else "Running..."
                    body_table.add_row(Text(f"\t{report_msg}", style="green"))
            body_table.add_row("")

            layout["body"].update(body_table)

            footer_table = Table.grid(expand=True)
            footer_table.add_column(justify="center", ratio=1)
            footer_table.add_row("[bold]Press Ctrl+Up to increase MAXIMUM_THREADS[/bold]")
            footer_table.add_row("[bold]Press Ctrl+Down to reduce MAXIMUM_THREADS[/bold]")
            footer_table.add_row("[bold]Press Ctrl+Left to disable CLOSE_THREAD_AFTER_TASK[/bold]")
            footer_table.add_row("[bold]Press Ctrl+Right to enable CLOSE_THREAD_AFTER_TASK[/bold]")
            footer_table.add_row("")
            footer_table.add_row("[bold]Press Ctrl+C to exit[/bold]")
            layout["footer"].update(footer_table)

            time.sleep(0.2)


def main(args):
    processor = Scheduler(path=args.root_path)

    if args.max_threads:
        processor.MAXIMUM_THREADS = args.max_threads

    try:
        if args.tui_mode:
            display_scheduler(processor)
            return
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        processor.stop()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='EventProcessorActivityMonitor',
        description='Monitors the file system events and run jobs as required.')
    parser.add_argument('-p', '--root-path', default=None, type=str,
                        help='Root path to listen to file events.')
    parser.add_argument('-threads', '--max-threads', default=None, type=int,
                        help='Maximum number of threads to utilize.')
    parser.add_argument('-tui', '--tui-mode', default=False, action='store_true',
                        help='Simple curses tui. Starts the monitor in graphical mode.')
    args = parser.parse_args()

    main(args)
