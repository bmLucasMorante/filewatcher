@ECHO OFF

rem Create | Activate | Update the virtual environment
python -m venv .venv
call .venv\Scripts\activate
python -m pip install --upgrade pip

rem Install requirements
pip install -r requirements.txt
