""" Sandbox file to prototype the app functionality

Statuses:
 - Published means that needs to be checked
 - Synced means the file has been found in our filesystem
 - Unsynced means the file is not in our filesystem
 - Archive means the file is ready or has been archived.
 - Good -- all its good
 - Corrupted -- broken file, need to get a generate a new file
 - Empty -- similar to corrupted, need to get a generate a new file

"""
import os
import time
import zipfile
import threading
import logging.handlers
from queue import Queue
from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import attr
import shotgun_api3

from typing import Union, Optional, Dict, List, Callable

SCRIPT_DIR = Path(__file__).parent.as_posix()
MONITOR_LOG_FILE = f"{SCRIPT_DIR}/monitor.log"
DEFAULT_ROOT_PATH = f"{SCRIPT_DIR}/test"
DEFAULT_MAXIMUM_THREADS = 1
SG_SERVER = "https://bidayamedia.shotgunstudio.com"
SG_SCRIPT = "BM-Task-Manager-Eventless"
SG_API_KEY = "icbnruj$Pvaizc2cmugmmjqfm"

PROJECT_NAME = "Mansour Series"
ARCHIVE_ROOT = "Z:/production/"
ZIP_EXTENSIONS = ['.zip', '.rar', '.7z']
TYPE_IMAGE = {'name': 'Image', 'id': 1}
TYPE_MOVIE = {'name': 'Movie', 'id': 34}
TYPE_PSD = {'name': 'Photoshop Scene', 'id': 100}
TYPE_HARMONY = {'name': 'Harmony Scene', 'id': 133}
TYPE_BLENDER = {'name': 'Blender Scene', 'id': 166}
TYPE_RENDERED_IMAGE = {'name': 'Rendered Image', 'id': 232}

STATUS_UNSYNCED = "Unsynced"
STATUS_PUBLISHED = "Published"
STATUS_SYNCED = "Synced"
STATUS_ARCHIVE = "Archive"
HEALTH_GOOD = "Good"
HEALTH_EMPTY = "Empty"
HEALTH_CORRUPTED = "Corrupted"

# Configure the logger
_logger = logging.getLogger('EventProcessorActivityMonitor')
_logger.setLevel(logging.INFO)
file_handler = logging.handlers.RotatingFileHandler(MONITOR_LOG_FILE, maxBytes=5*1024*1024, backupCount=5)
formatter = logging.Formatter(
    '%(asctime)s [%(levelname)s] - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
file_handler.setFormatter(formatter)
_logger.addHandler(file_handler)


# todo: change collector() to get the publishes that are either
#   sg_sync_status:STATUS_PUBLISHED or sg_sync_health:[HEALTH_CORRUPTED, HEALTH_EMPTY]
# fixme: limit MAXIMUM_THREADS based on system CPUs
# todo: make a utils.py file with the logger definition.
# todo: skip -- make collector yield each found file (we dont want to wait to do a full processing)
# todo: convert main() into a windows service that saves a .log
# todo: access the .log through the browser (also available in a file.)


def validate_zip(file, job: 'BaseJob' = None):
    job.report(f"Validating...")
    try:
        with zipfile.ZipFile(file, 'r') as zip_file:
            zip_file.testzip()
    except zipfile.BadZipFile:
        _logger.error("The file is not a zip file or is corrupted.")
        return False
    except zipfile.LargeZipFile:
        _logger.error("The zip file requires ZIP64 and is too large.")
        return False
    return True


def validate_unzipped_dir(destination: Optional[Union[Path, str]]):
    """Check that the destination folder is not empty"""
    if isinstance(destination, Path):
        destination = destination.as_posix()
    if not os.path.exists(destination):
        return False
    with os.scandir(destination) as entries:
        for entry in entries:
            if entry.is_file() or entry.is_dir():
                return True
    return False


def unzip(file: Union[Path, str], destination: Optional[Union[Path, str]] = None, job: 'BaseJob' = None) -> bool:
    if not isinstance(file, Path):
        file = Path(file)
    if destination is None:
        destination = file.parent
    elif not isinstance(destination, Path):
        destination = Path(destination)

    # Do unzipping
    job.report(f"Unzipping to {destination}")
    try:
        with zipfile.ZipFile(file.as_posix(), 'r') as zip_ref:
            zip_ref.extractall(destination)
    except Exception as e:
        _logger.exception(e, exc_info=True)
        return False
    return True


def update_sync_status(sg, job, status):
    publish_id = job.data['id']
    sync_status = status['sg_sync_status']
    sync_health = status['sg_sync_health']
    data_to_update = {
        "sg_sync_status": sync_status,
        "sg_sync_health": sync_health
        }

    job.report(f"Updating SG... ({sync_status}, {sync_health})")
    sg.update("PublishedFile",
              publish_id,
              data_to_update
              )


def cmd_unzip_renders(job, sg):
    zip_file: Path = job.data['file']

    status = {'sg_sync_status': None, 'sg_sync_health': None}
    if not zip_file.exists():
        if validate_unzipped_dir(zip_file.with_suffix('')):
            status['sg_sync_status'] = STATUS_SYNCED
            status['sg_sync_health'] = HEALTH_GOOD
        else:
            status['sg_sync_status'] = STATUS_UNSYNCED
            status['sg_sync_health'] = HEALTH_EMPTY
    elif not zip_file.stat().st_size:
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_EMPTY
    elif not validate_zip(zip_file, job=job):
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_CORRUPTED
    elif unzip(zip_file, destination=job.data['destination'], job=job):
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_GOOD
        job.report("Cleanup...")
        os.remove(zip_file)
    else:
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_CORRUPTED

    update_sync_status(sg, job, status)
    if status['sg_sync_health'] in [HEALTH_CORRUPTED, HEALTH_EMPTY]:
        return False


def cmd_zip_validate(job, sg):
    zip_file: Path = job.data['file']

    status = {'sg_sync_status': None, 'sg_sync_health': None}
    if not zip_file.exists():
        status['sg_sync_status'] = STATUS_UNSYNCED
        status['sg_sync_health'] = HEALTH_EMPTY
    elif not zip_file.stat().st_size:
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_EMPTY
    elif not validate_zip(zip_file, job=job):
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_CORRUPTED
    else:
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_GOOD

    update_sync_status(sg, job, status)


def cmd_generic(job, sg):
    file: Path = job.data['file']

    status = {'sg_sync_status': None, 'sg_sync_health': None}
    if not file.exists():
        status['sg_sync_status'] = STATUS_UNSYNCED
        status['sg_sync_health'] = HEALTH_EMPTY
    elif not file.stat().st_size:
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_EMPTY
    else:
        status['sg_sync_status'] = STATUS_SYNCED
        status['sg_sync_health'] = HEALTH_GOOD

    update_sync_status(sg, job, status)


@attr.s
class BaseJob:
    name = attr.ib(type=Optional[str], default=None)
    processed = attr.ib(type=Optional[bool], default=None)
    data = attr.ib(type=Optional[Dict], default=None)
    command = attr.ib(type=Optional[Callable], default=None)
    reports = attr.ib(type=List[str], factory=list)

    def execute(self, sg=None) -> bool:
        try:
            self.command(job=self, sg=sg) if self.command else None
        except Exception as e:
            _logger.exception(e)
            return False
        return True

    def report(self, msg):
        self.reports.append(msg)
        log_msg = f"@[{self.name}] {msg}"
        return _logger.info(log_msg)


class ZipFileEventHandler(FileSystemEventHandler):
    """
    Listens for file events under project root.
    Based on the fullpath of the event file, figure out the Publish and create BaseJob from there.
    """
    def __init__(self, scheduler):
        self.scheduler = scheduler
        self.skip_extensions = ['.exr']
        _logger.info(f"[{self.scheduler.OBSERVER_NAME_LISTENER}] Start")

    def on_created(self, event):
        # Skip directory events
        if not event or event.is_directory or not event.src_path:
            return
        self.process(event)

    def process(self, event):
        """
        Has to see if the created file is referenced in a Publish entity in SG
        If not, ignore event.
        If Publish found, build a BaseJob and add it to the queue.
        """
        # Raise error if no SG connection
        if not self.scheduler.sg:
            raise shotgun_api3.AuthenticationFault()

        file_path = Path(event.src_path)
        file_path_cache = file_path.as_posix().replace(ARCHIVE_ROOT, '')

        # Skip exr because they trigger some events when unzipping the exr_renders.
        # we can do this because know we only unzip exr renders.
        if file_path.suffix in self.skip_extensions:
            return

        filters = [
            ['path_cache', 'is', file_path_cache],
            ['project', 'name_is', PROJECT_NAME]
        ]

        fields = ['code', 'path', 'published_file_type']

        publish = self.scheduler.sg.find_one('PublishedFile', filters, fields)
        if not publish:
            _logger.info(f"No publish found with path_cache = {file_path_cache}")
            return

        job_data = {'id': publish['id'], 'file': file_path}
        if publish['published_file_type']['name'] == TYPE_RENDERED_IMAGE['name']:
            job_data['destination'] = file_path.parent
            job = BaseJob(data=job_data, command=cmd_unzip_renders)
        elif file_path.suffix in ZIP_EXTENSIONS:
            job = BaseJob(data=job_data, command=cmd_zip_validate)
        else:
            job = BaseJob(data=job_data, command=cmd_generic)

        job.name = file_path.name
        if job.name not in [j.name for j in self.scheduler.jobs.queue]:
            if self.is_fully_synced(file_path):
                self.scheduler.jobs.put(job)

    @staticmethod
    def is_fully_synced(file_path, timeout=300):
        initial_size = os.path.getsize(file_path)
        while True:
            time.sleep(1)
            current_size = os.path.getsize(file_path)
            if current_size == initial_size and time.time() - os.path.getmtime(file_path) >= timeout:
                return True
            initial_size = current_size

    @staticmethod
    def is_file_in_use(file_path):
        if not file_path.exists():
            return True
        try:
            file_path.rename(file_path)
        except PermissionError:
            return True
        else:
            return False


class Scheduler:
    CLOSE_THREAD_AFTER_TASK = False
    OBSERVER_NAME_LISTENER = 'FileSystemEventHandler'
    THREAD_NAME_DISTRIBUTOR = 'Distributor'
    THREAD_NAME_LEFTOVERS = 'Collector'
    THREAD_NAME_WORKER = 'Worker'

    def __init__(self, path: Optional[str] = None):
        _logger.info("="*120)
        _logger.info("[Event Processor Activity Monitor] Start")
        self._total_jobs_completed = list()
        self._failed_jobs = list()

        self.sg = self._init_sg()
        self.jobs = Queue()
        self.stop_event = threading.Event()

        self.ROOT_PATH = path if isinstance(path, str) else DEFAULT_ROOT_PATH
        self.MAXIMUM_THREADS = DEFAULT_MAXIMUM_THREADS
        self.ideal_threads = 0
        self.found_leftovers = list()
        self.threads = list()
        self.thread_jobs = dict()

        # Initialize the observer and start processing threads if path exists.
        if not os.path.exists(self.ROOT_PATH):
            raise ValueError(f"Root path doesn't exist {self.ROOT_PATH}")
        self.event_handler = ZipFileEventHandler(self)
        self.observer = Observer()
        self.observer.schedule(self.event_handler, self.ROOT_PATH, recursive=True)
        self.leftovers_thread = threading.Thread(target=self.collect_leftovers, name=self.THREAD_NAME_LEFTOVERS)
        self.distributor_thread = threading.Thread(target=self.distribute_jobs, name=self.THREAD_NAME_DISTRIBUTOR)

        # Start processing threads
        self.observer.start()
        self.leftovers_thread.start()
        self.distributor_thread.start()

    @staticmethod
    def _init_sg():
        """ Connect to ShotGrid """
        return shotgun_api3.Shotgun(SG_SERVER, script_name=SG_SCRIPT, api_key=SG_API_KEY)

    def collect_leftovers(self):
        _logger.info(f"[{self.THREAD_NAME_LEFTOVERS}] Start")
        collect_types = [TYPE_HARMONY, TYPE_RENDERED_IMAGE]  # [TYPE_PSD, TYPE_HARMONY, TYPE_RENDERED_IMAGE, TYPE_BLENDER, TYPE_IMAGE, TYPE_MOVIE]
        filters = [
            ['project', 'name_is', PROJECT_NAME],
            ['path_cache', 'is_not', ""],
            ['published_file_type', 'in', [{'type': "PublishedFileType", 'id': type['id']} for type in collect_types]],
            # ['sg_sync_health', 'in', [HEALTH_CORRUPTED, HEALTH_EMPTY]],  # [HEALTH_CORRUPTED, HEALTH_EMPTY]
            ['sg_sync_status', 'in', [STATUS_PUBLISHED]],  # [STATUS_PUBLISHED, STATUS_UNSYNCED]
        ]
        fields = ['code', 'path', 'path_cache', 'published_file_type']
        found_leftovers = self.sg.find('PublishedFile', filters, fields)

        for found_leftover in found_leftovers:
            file_path_cache = Path(found_leftover.get('path_cache'))
            file_path = Path(f"{ARCHIVE_ROOT}/{file_path_cache}")
            if not file_path.exists() and file_path.suffix not in ZIP_EXTENSIONS:
                _logger.info(f"{file_path.as_posix()} doest exists, skipping ")
                continue
            if file_path.name not in [j.name for j in self.jobs.queue]:
                job_data = {'id': found_leftover['id'], 'file': file_path}
                if found_leftover['published_file_type']['name'] == TYPE_RENDERED_IMAGE['name']:
                    job_data['destination'] = file_path.parent
                    job = BaseJob(data=job_data, command=cmd_unzip_renders)
                elif file_path.suffix in ZIP_EXTENSIONS:
                    job = BaseJob(data=job_data, command=cmd_zip_validate)
                else:
                    job = BaseJob(data=job_data, command=cmd_generic)
                job.name = file_path.name
                self.found_leftovers.append(found_leftover)
                self.jobs.put(job)

    def process_job(self, job: BaseJob, sg):
        job.processed = True
        try:
            _logger.info(f'[Job] {job.name} Executing: {job.command.__name__}')
            job.processed = job.execute(sg=sg)
        except Exception as e:
            _logger.exception(e)
            job.processed = False
        return job.processed

    def process_jobs(self, thread_name: str):
        sg = self._init_sg()
        while not self.stop_event.is_set():
            if self.jobs.empty():
                break

            job = self.jobs.get()
            self.thread_jobs[thread_name] = job
            if self.process_job(job, sg):
                self._total_jobs_completed.append(job)
            else:
                self._failed_jobs.append(job)
            self.thread_jobs[thread_name] = None
            self.jobs.task_done()
            if self.CLOSE_THREAD_AFTER_TASK:
                break

    def distribute_jobs(self):
        _logger.info(f"[{self.THREAD_NAME_DISTRIBUTOR}] Start")
        while not self.stop_event.is_set():
            self.ideal_threads = 0
            if self.jobs.unfinished_tasks:
                self.ideal_threads = self.MAXIMUM_THREADS
            # Spawn new threads if the queue size exceeds the threshold
            if len(self.threads) < self.ideal_threads and self.jobs.unfinished_tasks:
                thread_name = f"{self.THREAD_NAME_WORKER}-{len(self.threads)+1}"
                thread = self.spin_up_thread(name=thread_name)
                self.threads.append(thread)
            else:
                # Check if any threads can be joined (i.e., finished their work)
                for thread in self.threads[:]:
                    if not thread.is_alive():
                        self.threads.remove(thread)
                        if thread.name in self.thread_jobs:
                            self.thread_jobs.pop(thread.name)
            time.sleep(0.5)

    def spin_up_thread(self, name):
        thread = threading.Thread(target=self.process_jobs, args=(name,), name=name)
        thread.start()
        return thread

    def stop(self):
        _logger.info('Stopping threads and observer...')
        self.stop_event.set()
        self.leftovers_thread.join()
        for thread in self.threads:
            thread.join()
        self.observer.stop()
        self.observer.join()
        _logger.info('All threads and observer have been stopped.')

